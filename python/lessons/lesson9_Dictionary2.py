# СЛОВАРИ

enemy = {
            'loc_x': 70,
            'loc_y': 50,
            'color': 'green',
            'health': 100,
            'name': 'Mudillo',
            'image': ['image1.jpg', 'image2.jpg', 'image.jpg'] # массив в словаре
}

# Чтобы не плодить словари, воспользуемся массивом. Типо много мобов аналогичных
all_enemies = []

for x in range(0, 10):
    all_enemies.append(enemy.copy()) # .copy добавляет копию каждого моба в массив. Таким образом мы сможем изменить параметр у одного из мобов.

for y in all_enemies:
    print(y)

# меняем у 6го моба здоровье на 30
all_enemies[5]['health'] = 30 
    
for y in all_enemies:
    print(y)