# ВВОД ДАННЫХ ЮЗЕРА

name = input("Please enter your name: ")
print("Hi " + name)

num1 = input("Enter X: ")
num2 = input("Enter Y: ")

# конвертирование в числа
summa = int(num1) + int(num2) 
print(summa)

#########################################

# продолжать цикл, пока юзер не напишет слово sekret
message = ""

while True:
    message = input("Enter Password: ")
    if message == 'sekret':
        break
    else:
        print("\nPassword Not Correct")
print("Welcome")

########################################

# продолжать цикл, пока юзер не напишет стоп. Все ошибочные введенные данные он запишет в массив
mylist = []
msg = ""

while msg != 'stop':
    msg = input("Enter new item, and STOP to finish ")
    mylist.append(msg)

print(mylist)