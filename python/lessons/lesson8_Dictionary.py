# СЛОВАРИ

#---------------------------------------------
#            (----item----)
#           (key)    (value)
enemy = {
            'loc_x': 70,
            'loc_y': 50,
            'color': 'green',
            'health': 100,
            'name': 'Mudillo',
}

# показать определенную запись словаря
print("Location X = " +str(enemy['loc_x']))
print("Location Y = " +str(enemy['loc_y']))
print("His name is: " +str(enemy['name']))

# добавить запись в словарь
enemy['rank'] = 'Admiral' 
print("His rank is: " +str(enemy['rank']))

# удалить запись
del enemy['rank']

# меняем значения в записях + меняем с помощью if
enemy['loc_x'] = enemy['loc_x'] + 40
enemy['health'] = enemy['health'] - 30
if enemy['health'] < 80:
    enemy['color'] = 'yellow'

# показать keys и value
print(enemy.keys())
print(enemy.values())