# РАБОТА СО СТРОЧКАМИ

mystring = "bla bla bla"

name = "vasya pupkin"

print(name.title()) #title ставить заглавные буквы, также оставльные делает незаглавными
print(name.upper()) #upper все буквы заглавные
print(name.lower()) #lower все буквы незаглавные

print("Privet stroka nomer 1\nPrivet stroka nomer 2\n\nStroka nomer 3") #\n перенос строки
print("Messages:\n\tMessage1\n\tMessage2\n\tMessage3\nEND") #\t табуляция

print("Lower name: " + name.lower())

a = " . ,dadya vasya . "
print(a)
print(a.rstrip()) #rstrip стерает пробелы в конце переменной
print(a.lstrip()) #lstrip стерает пробелы в начале переменной
print(a.strip()) #lstrip стерает пробелы с двух сторон переменной

b = ".....  dadya vasya  ....."
print(b.strip(".")) #strip(".") стерает точки, которые указали в скобках, со двух сторон переменной

###############
b = b.strip(".") # удаление точек
b = b.strip() # удаление пробелов
print(b) 
###############