# РАБОТА С МАССИВАМИ ЧЕРЕЗ ЦИКЛЫ

cars = ['bmw', 'vw', 'seat', 'skoda', 'lada']

for x in cars: # показать записи через цикл
    print(x)

for x in range(1, 6): # показать номера чисел через цикл
    print(x)

mycars = cars[1:3] # показать записи от 2ой до 4ой
print(mycars)

mycars = cars[:4] # показать записи от начала до 5ой
print(mycars)

mycars = cars[-3:-1] # показать записи от 3ей до 5ой
print(mycars)

################################################################

mynumber_list = list(range(0, 10)) # массив из чисел
print(mynumber_list)

print(str(max(mynumber_list))) # показать максимальное значение в массиве
print(str(min(mynumber_list))) # показать минимальное значение в массиве
print(str(sum(mynumber_list))) # показать сумму значений в массиве

################################################################


