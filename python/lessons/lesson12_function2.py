# ФУНКЦИИ 2

################################################
# ФУНКЦИИ И СЛОВАРИ

def create_record(name, telephone, address):
    """Create Record"""
    record = {
        'name': name,
        'phone': telephone,
        'address': address
    }
    return record

user1 = create_record("Vasya", "+7909999999", "Trt")
user2 = create_record("Dima", "+7909994359", "Trttwa")

print(user1)
print(user2)

################################################
# ФУНКЦИЯ С НЕОПРЕДЕЛЕННЫМ КОЛ-ВОМ ПАРАМЕТРОВ

def give_award(medal, *persons): # * ставим для неопределенного кол-ва параметров. Должен стоять в конце определения
    """Give Medals to persons"""
    for person in persons:
        print("Tovarish " + person + " nagrajdaetsa medliyu " + medal)
    


give_award("Za otvagu", "Tanya", "Petya")
give_award("Za trud", "Sveta", "Vasya", "Kirya")