# ПЕРЕМЕННЫЕ

a = "hello"
b = 25

print(a)
print(b)

f_name = "Ivan"
l_name = "Ivanov"
age = 35
full_name = (f_name + " " + l_name)

print(full_name + " " + str(age)) # т.к. прибавить к строчке число невозможно без str

f_name = "ivan"
F_NAME = "IVAN"

print(f_name + "---------" + F_NAME)

num1 = 111111111111111111111
num2 = 222222222222222222222
num3 = num1 + num2

print(num3)