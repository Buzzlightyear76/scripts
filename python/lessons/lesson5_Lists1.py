# РАБОТА С МАССИВАМИ

cities = ['New York', 'Moscow', 'new dehli', 'Sim', 'Toronto' ] # массив

print(cities) # показать весь массив
print(len(cities)) # показать длинну массива
print(cities[0]) # показать первый объект массива
print(cities[-1]) # показать последний объект массива
print(cities[-2]) # показать предпоследний объект массива
print(cities[2].upper()) # все заглавные буквы в записи

cities[2] = 'Tula' # заменили третью запись в массиве
print(cities)

cities.append('Kursk') # добавить запись в конец массива
print(cities)

cities.insert(0, 'Sochi') # добавить запись в начало массива
print(cities)

cities.insert(2, 'Sochi') # добавить запись в массив после второй записи
print(cities)

del cities[-1] # удалить последнюю запись
print(cities)

cities.remove('Tula') # удалить запись по имени
print(cities)

deleted_city = cities.pop() # еще способ удалить запись. В скобки указываем номер записи, иначе удалит последний
print("Deleted city is: " + deleted_city)

cities.sort() # сортировка записей по алфавиту
print(cities)

cities.sort(reverse=True) # сортировка записей по алфавиту в обратную сторону
print(cities)

cities.reverse() # показать записи в обратном порядке
print(cities)

