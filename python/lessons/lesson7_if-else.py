# УСЛОВНЫЕ ОПЕРАТОРЫ

x = 26

if x == 25:
    print("YES you Right")
else:
    print("NO you are wrong")

######################################

age = 20

if (age <= 4):
    print("You are baby!")
elif (age > 4) and (age < 12):
    print("You are kid")
elif (age >=12 ) and (age < 19):
    print("You are teenager")
else:
    print("You are stariy perdun")

######################################

all_cars = ['bmw', 'vw', 'seat', 'skoda', 'lada']

if 'lada' in all_cars:
    print("YES lada in the list")
else:
    print("lada not in the list")

######################################

all_cars = ['bmw', 'vw', 'seat', 'skoda', 'lada']
german_cars = ['bmw', 'vw']

for x in all_cars:
    if x in german_cars:
        print(x + " is German car")
    else:
        print(x + " is not German car") 