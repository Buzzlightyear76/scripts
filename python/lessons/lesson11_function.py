# ФУНКЦИИ

# Используются для повтора команд в нескольких местах кода

def priveti():
    """Print priveti""" # доп.инфо, необязательно
    print("Congratulations, wish you all the beast!")
    print("Hello Hello")


def aaa():
    print("AAA")


print("--------------------")
priveti()
priveti()
aaa()

#######################################################

def priveti(name):
    """Print priveti""" # доп.инфо, необязательно
    print("Congratulations, " + name + " wish you all the beast!")

priveti("Vasya")

#######################################################
# ВВОД ДАННЫХ ЮЗЕРА

name = input("Please enter your name: ")

def priveti():
    """Print priveti""" # доп.инфо, необязательно
    print("Congratulations, " + name + " wish you all the beast!")

priveti()

#######################################################
# ЧИСЛА

def summa(x, y):
    print(x+y)

summa(10, 20)

################

def summa(x, y):
    return x+y

x = summa(10, 20)
print(x)

#######################################################
# ФАКТОР

def factorial(x):
    """Calculate Factorial of number X"""
    otvet = 1 # начинаем счет всегда с единицы
    for i in range(1, x + 1):
        otvet = otvet * i
    return otvet

print(factorial(1))
print(factorial(5))


# 2! = 1 *2
# 3! = 1 *2 *3
# 4! = 1 *2 *3 *4

