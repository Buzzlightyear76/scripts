#!/bin/bash

echo -e "\033[33mВыберите действие"
echo "1.Добавить запись"
echo "2.Показать записи"
echo "3.Удалить все данные из списка"

read menu
file="info.txt"

if [[ $menu == 1 ]]; then
  echo "Введите имя"
  read name

  echo "Введите фамилию"
  read family

  echo "Введите возраст"
  read age

  comment=""
  check=1

  if [[ $family == "Гитлер" && $name == "Адольф" ]]; then
    comment="ZiPfile"
      elif [[ $age -gt 100 ]]; then
        comment="врунишка"
  fi

  for var in $(cat $file)
  do
    if [[ $var == $name";"$family";"$age";"$comment ]]; then
      echo -e "\033[31mОшибка:Такая запись уже существует"
      check=0
      break
    fi
  done

  if [[ $check == 1 ]]; then
    echo $name";"$family";"$age";"$comment >> info.txt
  fi

elif [[ $menu == 2 ]]; then
  echo -e "\033[36m"
  cat $file | sed 's/;/ /g'
elif [[ $menu == 3 ]]; then
  echo "Вы уверены? [Д]/[Н]"
  read question
  if [[ $question ==  "Д" ]]; then
    > $file
  elif [[ $question ==  "Н" ]]; then
    echo "Определись уже"
  else
  echo -e "\033[31mОшибка:Некорректный запрос"
  fi
else
  echo -e "\033[31mОшибка:Некорректный запрос"
fi