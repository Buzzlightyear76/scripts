#!/bin/bash


#configs_test
#extraction_api="/mnt/c/Users/idoida/scripts/test_config/findface-extraction-api.txt"
#findface_security="/mnt/c/Users/idoida/scripts/test_config/config.txt"
#video_worker_gpu="/mnt/c/Users/idoida/scripts/test_config/video_worker_gpu.txt"
#video_worker_cpu="/mnt/c/Users/idoida/scripts/test_config/video_worker_cpu.txt"

#configs
extraction_api="/etc/findface-extraction-api.ini"
findface_security="/etc/findface-security/config.py"
video_worker_gpu="/etc/findface-video-worker-gpu.ini"
video_worker_cpu="/etc/findface-video-worker-cpu.ini"

replace()
{
	sed -i "/$1:/s/$1:.*/$1: $2/g" $3
}

replace2()
{
	sed -i "/^.*$1:/{n;s/model:.*/model: $2/}" $3
}

replace3()
{
	sed -i "/^.*$1:/{n;n;s/quality_attribute:.*/quality_attribute: $2/}" $3
}


#menu
echo -e "\033[36mВы можете добавить(1) или отключить атрибуты(2)."
PS3="Укажите вариант: "
tput sgr0
select menu in add delete; do
	case $menu in
		add)
			select menu_add in face car body; do
				case $menu_add in
				##########################################################_FACE_#######################################################################
					face)
						echo -e "\033[36mУстановка для CPU или GPU?"
						PS3="Укажите вариант: "
						tput sgr0
						select gpucpu in CPU GPU; do
							case $gpucpu in
								CPU)
									#extraction_api_models_extractors
                                    variants=('age' 'gender' 'emotions' 'beard' 'glasses' 'medmask')
                                    echo "Введите номера вариантов через пробел. По умолчанию вариант 0 - all"
                                    for i in ${!variants[@]}; do
                                        echo $(($i+1))") ${variants[i]}"
                                    done
									
									echo $((0))") all"

                                    read choise
                                    IFS=' ' read -ra choises <<< $choise
									
									if [[ $choise == 0 ]]; then
										for i in ${!variants[@]}; do
											choises[i]=$(($i+1))
										done
									fi
										
                                    features=()
                                    for i in ${!choises[@]}; do
										
										if [[ ${choises[i]} == 1 ]]; then
                                            replace "face_age" "faceattr\/age.v2.cpu.fnk" $extraction_api  
                                            features[i]="'age'"
											continue
										fi

                                        if [[ ${choises[i]} == 2 ]]; then
                                            replace "face_gender" "faceattr\/gender.v2.cpu.fnk" $extraction_api 
                                            features[i]="'gender'"
											continue
                                        fi

                                        if [[ ${choises[i]} == 3 ]]; then
                                            replace "face_emotions" "faceattr\/emotions.v1.cpu.fnk" $extraction_api 
                                            features[i]="'emotions'"
                                            continue
                                        fi

                                        if [[ ${choises[i]} == 4 ]]; then
                                            replace "face_beard" "faceattr\/beard.v0.cpu.fnk" $extraction_api 
                                            features[i]="'beard'"
                                            continue
                                        fi

                                        if [[ ${choises[i]} == 5 ]]; then
                                            replace "face_glasses3" "faceattr\/glasses3.v0.cpu.fnk" $extraction_api 
                                            features[i]="'glasses'"
                                            continue
                                        fi

                                        if [[ ${choises[i]} == 6 ]]; then
                                            replace "face_medmask3" "faceattr\/medmask3.v2.cpu.fnk" $extraction_api 
                                            features[i]="'medmask'"
                                            continue
                                        fi

									done
                                    
									#findface_security
                                    features_str=""
                                    first=1
                                    for i in ${!features[@]}; do
                                        if [[ $first == 1 ]]; then
                                            features_str="${features[i]}"
                                            first=0
                                            continue
                                        fi
                                       features_str="$features_str, ${features[i]}"
                                    done
                                    
                                    echo $features_str
                                    replace "'FACE_EVENTS_FEATURES'" "[$features_str]," $findface_security
                                    systemctl restart findface-extraction-api
                                    systemctl restart findface-security.service

									break
									;;

								GPU)
									#extraction_api_models_extractors
                                    variants=('age' 'gender' 'emotions' 'beard' 'glasses' 'medmask')
                                    echo "Введите номера вариантов через пробел. По умолчанию вариант 0 - all"
                                    for i in ${!variants[@]}; do
                                        echo $(($i+1))") ${variants[i]}"
                                    done
									
									echo $((0))") all"

                                    read choise
                                    IFS=' ' read -ra choises <<< $choise
									
									if [[ $choise == 0 ]]; then
										for i in ${!variants[@]}; do
											choises[i]=$(($i+1))
										done
									fi
										
                                    features=()
                                    for i in ${!choises[@]}; do
										
										if [[ ${choises[i]} == 1 ]]; then
                                            replace "face_age" "faceattr\/age.v2.gpu.fnk" $extraction_api  
                                            features[i]="'age'"
											continue
										fi

                                        if [[ ${choises[i]} == 2 ]]; then
                                            replace "face_gender" "faceattr\/gender.v2.gpu.fnk" $extraction_api 
                                            features[i]="'gender'"
											continue
                                        fi

                                        if [[ ${choises[i]} == 3 ]]; then
                                            replace "face_emotions" "faceattr\/emotions.v1.gpu.fnk" $extraction_api 
                                            features[i]="'emotions'"
                                            continue
                                        fi

                                        if [[ ${choises[i]} == 4 ]]; then
                                            replace "face_beard" "faceattr\/beard.v0.gpu.fnk" $extraction_api 
                                            features[i]="'beard'"
                                            continue
                                        fi

                                        if [[ ${choises[i]} == 5 ]]; then
                                            replace "face_glasses3" "faceattr\/glasses3.v0.gpu.fnk" $extraction_api 
                                            features[i]="'glasses'"
                                            continue
                                        fi

                                        if [[ ${choises[i]} == 6 ]]; then
                                            replace "face_medmask3" "faceattr\/medmask3.v2.gpu.fnk" $extraction_api 
                                            features[i]="'medmask'"
                                            continue
                                        fi

									done


									#findface_security
                                    features_str=""
                                    first=1
                                    for i in ${!features[@]}; do
                                        if [[ $first == 1 ]]; then
                                            features_str="${features[i]}"
                                            first=0
                                            continue
                                        fi
                                       features_str="$features_str, ${features[i]}"
                                    done
									
                                    #function join_by { local IFS="$1"; shift; echo "$*"; }
                                    #features_str=join_by ", " "${features[@]}" 
                                    
                                    echo $features_str
                                    replace "'FACE_EVENTS_FEATURES'" "[$features_str]," $findface_security
                                    systemctl restart findface-extraction-api
                                    systemctl restart findface-security.service
									break
									;;

							esac
						done	
					;;
				##########################################################_CAR_########################################################################
					car)
						echo -e "\033[36mУстановка для CPU или GPU?"
						PS3="Укажите вариант: "
						tput sgr0
						select gpucpu in CPU GPU; do
							case $gpucpu in
								CPU)
									#extraction_api_models_extractors
                                    variants=('description' 'emben' 'license_plate' 'license_plate_quality' 'quality' 'special_types')
                                    echo "Введите номера вариантов через пробел. По умолчанию вариант 0 - all"
                                    for i in ${!variants[@]}; do
                                        echo $(($i+1))") ${variants[i]}"
                                    done
									
									echo $((0))") all"

                                    read choise
                                    IFS=' ' read -ra choises <<< $choise
									
									if [[ $choise == 0 ]]; then
										for i in ${!variants[@]}; do
											choises[i]=$(($i+1))
										done
									fi
										
                                    features=()
                                    for i in ${!choises[@]}; do
										
										if [[ ${choises[i]} == 1 ]]; then
                                            replace "car_description" "carattr\/description.v0.cpu.fnk" $extraction_api  
                                            features[i]="'description'"
											continue
										fi

										if [[ ${choises[i]} == 2 ]]; then
                                            replace "car_emben" "carrec\/alonso.cpu.fnk" $extraction_api  
											continue
										fi
										
										if [[ ${choises[i]} == 3 ]]; then
                                            replace "car_license_plate" "carattr\/carattr.license_plate.v4.cpu.fnk" $extraction_api  
                                            features[i]="'license_plate'"
											continue
										fi

										if [[ ${choises[i]} == 4 ]]; then
                                            replace "car_license_plate_quality" "carattr\/carattr.license_plate_quality.v0.cpu.fnk" $extraction_api  
											continue
										fi

										if [[ ${choises[i]} == 5 ]]; then
                                            replace "car_quality" "carattr\/carattr.quality.v0.cpu.fnk" $extraction_api  
											continue
										fi

										if [[ ${choises[i]} == 6 ]]; then
                                            replace "car_special_types" "carattr\/carattr.special_types.v0.cpu.fnk" $extraction_api  
                                            features[i]="'special_vehicle_type'"
											continue
										fi
									done
									
									#clean_extraction_api_detectors
									sed -i '/efreitor:/,/.*2048x2048/d' $extraction_api

									#add_extraction_api_detectors
									sed -i '/objects:/i\\x20\x20\x20\x20efreitor:\x0a\x20\x20\x20\x20\x20\x20aliases:\x0a\x20\x20\x20\x20\x20\x20- car\x0a\x20\x20\x20\x20\x20\x20model: cadet/efreitor.cpu.fnk' $extraction_api
									sed -i '/objects:/i\\x20\x20\x20\x20\x20\x20options:\x0a\x20\x20\x20\x20\x20\x20\x20\x20min_object_size: 32' $extraction_api
									sed -i '/objects:/i\\x20\x20\x20\x20\x20\x20\x20\x20resolutions: [256x256, 384x384, 512x512, 768x768, 1024x1024, 1536x1536, 2048x2048]' $extraction_api

									#extraction_api_normalizers
									replace2 "cropbbox" "facenorm\/cropbbox.v2.cpu.fnk" $extraction_api
									replace2 "carlicplate" "carnorm\/anaferon.v3.cpu.fnk" $extraction_api
									replace3 "car" "car_quality" $extraction_api
									systemctl restart findface-extraction-api
								
									#video_worker_cpu
									sed -i '/^.*CFG_CAR_MIN_SIZE/{n;s/min_size.*/min_size = 60/}' $video_worker_cpu
									sed -i '/^.*CFG_CAR_DETECTOR/{n;s/detector.*/detector = \/usr\/share\/findface-data\/models\/cadet\/efreitor.cpu.fnk/}' $video_worker_cpu
									sed -i '/^.*CFG_CAR_NORM longopt:/{n;s/norm.*/norm = \/usr\/share\/findface-data\/models\/facenorm\/cropbbox.v2.cpu.fnk/}' $video_worker_cpu
									sed -i '/^.*CFG_CAR_QUALITY/{n;s/quality.*/quality = \/usr\/share\/findface-data\/models\/carattr\/carattr.quality.v0.cpu.fnk/}' $video_worker_cpu
									sed -i '/^.*CFG_CAR_NORM_QUALITY/{n;s/norm_quality.*/norm_quality = \/usr\/share\/findface-data\/models\/facenorm\/cropbbox.v2.cpu.fnk/}' $video_worker_cpu
									systemctl restart findface-video-worker-сpu.service

									#findface_security
                                    features_str=""
                                    first=1
                                    for i in ${!features[@]}; do
                                        if [[ $first == 1 ]]; then
                                            features_str="${features[i]}"
                                            first=0
                                            continue
                                        fi
                                       features_str="$features_str, ${features[i]}"
                                    done
									
                                    echo $features_str
                                    
									replace "'CAR_EVENTS_FEATURES'" "[$features_str]," $findface_security
									replace "'ENABLE_CARS'" "True," $findface_security
									sed -i "/^.*EXTRA_CAR_MATCHING/{n;s/'color':.*/'color': {'enabled': True, 'min_confidence': 0},/}" $findface_security
									sed -i "/^.*EXTRA_CAR_MATCHING/{n;n;s/'body':.*/'body': {'enabled': True, 'min_confidence': 0},/}" $findface_security
									sed -i "/^.*EXTRA_CAR_MATCHING/{n;n;n;s/'make':.*/'make': {'enabled': True, 'min_confidence': 0},/}" $findface_security
									sed -i "/^.*EXTRA_CAR_MATCHING/{n;n;n;n;s/'model':.*/'model': {'enabled': True, 'min_confidence': 0},/}" $findface_security
									sed -i "/^.*EXTRA_CAR_MATCHING/{n;n;n;n;n;s/'special_vehicle_type':.*/'special_vehicle_type': {'enabled': True, 'min_confidence': 0}/}" $findface_security
									systemctl restart findface-security.service
									break
								;;

								GPU)
									#extraction_api_models_extractors
                                    variants=('description' 'emben' 'license_plate' 'license_plate_quality' 'quality' 'special_types')
                                    echo "Введите номера вариантов через пробел. По умолчанию вариант 0 - all"
                                    for i in ${!variants[@]}; do
                                        echo $(($i+1))") ${variants[i]}"
                                    done
									
									echo $((0))") all"

                                    read choise
                                    IFS=' ' read -ra choises <<< $choise
									
									if [[ $choise == 0 ]]; then
										for i in ${!variants[@]}; do
											choises[i]=$(($i+1))
										done
									fi
										
                                    features=()
                                    for i in ${!choises[@]}; do
										
										if [[ ${choises[i]} == 1 ]]; then
                                            replace "car_description" "carattr\/description.v0.gpu.fnk" $extraction_api  
                                            features[i]="'description'"
											continue
										fi

										if [[ ${choises[i]} == 2 ]]; then
                                            replace "car_emben" "carrec\/alonso.gpu.fnk" $extraction_api  
											continue
										fi
										
										if [[ ${choises[i]} == 3 ]]; then
                                            replace "car_license_plate" "carattr\/carattr.license_plate.v4.gpu.fnk" $extraction_api  
                                            features[i]="'license_plate'"
											continue
										fi

										if [[ ${choises[i]} == 4 ]]; then
                                            replace "car_license_plate_quality" "carattr\/carattr.license_plate_quality.v0.gpu.fnk" $extraction_api  
											continue
										fi

										if [[ ${choises[i]} == 5 ]]; then
                                            replace "car_quality" "carattr\/carattr.quality.v0.gpu.fnk" $extraction_api  
											continue
										fi

										if [[ ${choises[i]} == 6 ]]; then
                                            replace "car_special_types" "carattr\/carattr.special_types.v0.gpu.fnk" $extraction_api  
                                            features[i]="'special_vehicle_type'"
											continue
										fi
									done

									#clean_extraction_api_detectors
									sed -i '/efreitor:/,/.*2048x2048/d' $extraction_api

									#add_extraction_api_detectors
									sed -i '/objects:/i\\x20\x20\x20\x20efreitor:\x0a\x20\x20\x20\x20\x20\x20aliases:\x0a\x20\x20\x20\x20\x20\x20- car\x0a\x20\x20\x20\x20\x20\x20model: cadet/efreitor.gpu.fnk' $extraction_api
									sed -i '/objects:/i\\x20\x20\x20\x20\x20\x20options:\x0a\x20\x20\x20\x20\x20\x20\x20\x20min_object_size: 32' $extraction_api
									sed -i '/objects:/i\\x20\x20\x20\x20\x20\x20\x20\x20resolutions: [256x256, 384x384, 512x512, 768x768, 1024x1024, 1536x1536, 2048x2048]' $extraction_api

									#extraction_api_normalizers
									replace2 "cropbbox" "facenorm\/cropbbox.v2.gpu.fnk" $extraction_api
									replace2 "carlicplate" "carnorm\/anaferon.v3.gpu.fnk" $extraction_api
									replace3 "car" "car_quality" $extraction_api
									systemctl restart findface-extraction-api
								
									#video_worker_cpu
									sed -i '/^.*CFG_CAR_MIN_SIZE/{n;s/min_size.*/min_size = 60/}' $video_worker_gpu
									sed -i '/^.*CFG_CAR_DETECTOR/{n;s/detector.*/detector = \/usr\/share\/findface-data\/models\/cadet\/efreitor.gpu.fnk/}' $video_worker_gpu
									sed -i '/^.*CFG_CAR_NORM longopt:/{n;s/norm.*/norm = \/usr\/share\/findface-data\/models\/facenorm\/cropbbox.v2.gpu.fnk/}' $video_worker_gpu
									sed -i '/^.*CFG_CAR_QUALITY/{n;s/quality.*/quality = \/usr\/share\/findface-data\/models\/carattr\/carattr.quality.v0.gpu.fnk/}' $video_worker_gpu
									sed -i '/^.*CFG_CAR_NORM_QUALITY/{n;s/norm_quality.*/norm_quality = \/usr\/share\/findface-data\/models\/facenorm\/cropbbox.v2.gpu.fnk/}' $video_worker_gpu

									systemctl restart findface-video-worker-gpu.service

									#findface_security
                                    features_str=""
                                    first=1
                                    for i in ${!features[@]}; do
                                        if [[ $first == 1 ]]; then
                                            features_str="${features[i]}"
                                            first=0
                                            continue
                                        fi
                                       features_str="$features_str, ${features[i]}"
                                    done
									
                                    echo $features_str
                                    
									replace "'CAR_EVENTS_FEATURES'" "[$features_str]," $findface_security
									replace "'ENABLE_CARS'" "True," $findface_security
									sed -i "/^.*EXTRA_CAR_MATCHING/{n;s/'color':.*/'color': {'enabled': True, 'min_confidence': 0},/}" $findface_security
									sed -i "/^.*EXTRA_CAR_MATCHING/{n;n;s/'body':.*/'body': {'enabled': True, 'min_confidence': 0},/}" $findface_security
									sed -i "/^.*EXTRA_CAR_MATCHING/{n;n;n;s/'make':.*/'make': {'enabled': True, 'min_confidence': 0},/}" $findface_security
									sed -i "/^.*EXTRA_CAR_MATCHING/{n;n;n;n;s/'model':.*/'model': {'enabled': True, 'min_confidence': 0},/}" $findface_security
									sed -i "/^.*EXTRA_CAR_MATCHING/{n;n;n;n;n;s/'special_vehicle_type':.*/'special_vehicle_type': {'enabled': True, 'min_confidence': 0}/}" $findface_security
									systemctl restart findface-security.service
									break
								;;
							esac
						done	
					;;
				##########################################################_BODY_#######################################################################

					body)
						echo -e "\033[36mУстановка для CPU или GPU?"
						PS3="Укажите вариант: "
						tput sgr0
						select gpucpu in CPU GPU; do
							case $gpucpu in
								CPU)
									#extraction_api_extractors_models									
									variants=('clothes' 'color' 'emben' 'quality')
                                    echo "Введите номера вариантов через пробел. По умолчанию вариант 0 - all"
                                    for i in ${!variants[@]}; do
                                        echo $(($i+1))") ${variants[i]}"
                                    done
									
									echo $((0))") all"

                                    read choise
                                    IFS=' ' read -ra choises <<< $choise
									
									if [[ $choise == 0 ]]; then
										for i in ${!variants[@]}; do
											choises[i]=$(($i+1))
										done
									fi
										
                                    features=()
                                    for i in ${!choises[@]}; do
										
										if [[ ${choises[i]} == 1 ]]; then
                                            replace "body_clothes" "pedattr\/pedattr.clothes_type.v0.cpu.fnk" $extraction_api  
                                            features[i]="'clothes'"
											continue
										fi

										if [[ ${choises[i]} == 2 ]]; then
                                            replace "body_color" "pedattr\/pedattr.color.v1.cpu.fnk" $extraction_api  
											features[i]="'color'"
											continue
										fi
										
										if [[ ${choises[i]} == 3 ]]; then
                                            replace "body_emben" "pedrec\/andariel.cpu.fnk" $extraction_api  
											continue
										fi

										if [[ ${choises[i]} == 4 ]]; then
                                            replace "body_quality" "pedattr\/pedattr.quality.v0.cpu.fnk" $extraction_api  
											continue
										fi

									done
									
									#clean_detectors
									sed -i '/glenn:/,/.*2048x2048/d' $extraction_api

									#add_extraction_api_detectors
									sed -i '/objects:/i\\x20\x20\x20\x20glenn:\x0a\x20\x20\x20\x20\x20\x20aliases:\x0a\x20\x20\x20\x20\x20\x20- body\x0a\x20\x20\x20\x20\x20\x20- silhouette' $extraction_api
									sed -i '/objects:/i\\x20\x20\x20\x20\x20\x20model: pedet\/glenn_005.cpu.fnk\x0a\x20\x20\x20\x20\x20\x20options:\x0a\x20\x20\x20\x20\x20\x20\x20\x20min_object_size: 32' $extraction_api
									sed -i '/objects:/i\\x20\x20\x20\x20\x20\x20\x20\x20resolutions: [256x256, 384x384, 512x512, 768x768, 1024x1024, 1536x1536, 2048x2048]' $extraction_api
									
									#extraction_api_normalizers
									replace2 "cropbbox" "facenorm\/cropbbox.v2.cpu.fnk" $extraction_api
									replace3 "body" "body_quality" $extraction_api
									systemctl restart findface-extraction-api

									#video_worker_cpu
									sed -i '/^.*CFG_BODY_MIN_SIZE/{n;s/min_size.*/min_size = 60/}' $video_worker_cpu
									sed -i '/^.*CFG_BODY_DETECTOR/{n;s/detector.*/detector = \/usr\/share\/findface-data\/models\/pedet\/glenny_005_fast.cpu.fnk/}' $video_worker_cpu
									sed -i '/^.*CFG_BODY_NORM longopt:/{n;s/norm.*/norm = \/usr\/share\/findface-data\/models\/facenorm\/cropbbox.v2.cpu.fnk/}' $video_worker_cpu
									sed -i '/^.*CFG_BODY_QUALITY/{n;s/quality.*/quality = \/usr\/share\/findface-data\/models\/pedattr\/pedattr.quality.v0.cpu.fnk/}' $video_worker_cpu
									sed -i '/^.*CFG_BODY_NORM_QUALITY/{n;s/norm_quality.*/norm_quality = \/usr\/share\/findface-data\/models\/facenorm\/cropbbox.v2.cpu.fnk/}' $video_worker_cpu

									systemctl restart findface-video-worker-cpu.service

									#findface_security
									features_str=""
                                    first=1
                                    for i in ${!features[@]}; do
                                        if [[ $first == 1 ]]; then
                                            features_str="${features[i]}"
                                            first=0
                                            continue
                                        fi
                                       features_str="$features_str, ${features[i]}"
                                    done
									
                                    echo $features_str
									replace "'BODY_EVENTS_FEATURES'" "[$features_str]," $findface_security
									replace "'ENABLE_BODIES'" "True," $findface_security
									systemctl restart findface-security.service
									break
								;;
								
								GPU)
									#extraction_api_extractors_models									
									variants=('clothes' 'color' 'emben' 'quality')
                                    echo "Введите номера вариантов через пробел. По умолчанию вариант 0 - all"
                                    for i in ${!variants[@]}; do
                                        echo $(($i+1))") ${variants[i]}"
                                    done
									
									echo $((0))") all"

                                    read choise
                                    IFS=' ' read -ra choises <<< $choise
									
									if [[ $choise == 0 ]]; then
										for i in ${!variants[@]}; do
											choises[i]=$(($i+1))
										done
									fi
										
                                    features=()
                                    for i in ${!choises[@]}; do
										
										if [[ ${choises[i]} == 1 ]]; then
                                            replace "body_clothes" "pedattr\/pedattr.clothes_type.v0.gpu.fnk" $extraction_api  
                                            features[i]="'clothes'"
											continue
										fi

										if [[ ${choises[i]} == 2 ]]; then
                                            replace "body_color" "pedattr\/pedattr.color.v1.gpu.fnk" $extraction_api  
											features[i]="'color'"
											continue
										fi
										
										if [[ ${choises[i]} == 3 ]]; then
                                            replace "body_emben" "pedrec\/andariel.gpu.fnk" $extraction_api  
											continue
										fi

										if [[ ${choises[i]} == 4 ]]; then
                                            replace "body_quality" "pedattr\/pedattr.quality.v0.gpu.fnk" $extraction_api  
											continue
										fi

									done
									
									#clean_detectors
									sed -i '/glenn:/,/.*2048x2048/d' $extraction_api

									#add_extraction_api_detectors
									sed -i '/objects:/i\\x20\x20\x20\x20glenn:\x0a\x20\x20\x20\x20\x20\x20aliases:\x0a\x20\x20\x20\x20\x20\x20- body\x0a\x20\x20\x20\x20\x20\x20- silhouette' $extraction_api
									sed -i '/objects:/i\\x20\x20\x20\x20\x20\x20model: pedet\/glenn_005.gpu.fnk\x0a\x20\x20\x20\x20\x20\x20options:\x0a\x20\x20\x20\x20\x20\x20\x20\x20min_object_size: 32' $extraction_api
									sed -i '/objects:/i\\x20\x20\x20\x20\x20\x20\x20\x20resolutions: [256x256, 384x384, 512x512, 768x768, 1024x1024, 1536x1536, 2048x2048]' $extraction_api
									
									#extraction_api_normalizers
									replace2 "cropbbox" "facenorm\/cropbbox.v2.gpu.fnk" $extraction_api
									replace3 "body" "body_quality" $extraction_api
									systemctl restart findface-extraction-api

									#video_worker_gpu
									sed -i '/^.*CFG_BODY_MIN_SIZE/{n;s/min_size.*/min_size = 60/}' $video_worker_gpu
									sed -i '/^.*CFG_BODY_DETECTOR/{n;s/detector.*/detector = \/usr\/share\/findface-data\/models\/pedet\/glenny_005_fast.gpu.fnk/}' $video_worker_gpu
									sed -i '/^.*CFG_BODY_NORM longopt:/{n;s/norm.*/norm = \/usr\/share\/findface-data\/models\/facenorm\/cropbbox.v2.gpu.fnk/}' $video_worker_gpu
									sed -i '/^.*CFG_BODY_QUALITY/{n;s/quality.*/quality = \/usr\/share\/findface-data\/models\/pedattr\/pedattr.quality.v0.gpu.fnk/}' $video_worker_gpu
									sed -i '/^.*CFG_BODY_NORM_QUALITY/{n;s/norm_quality.*/norm_quality = \/usr\/share\/findface-data\/models\/facenorm\/cropbbox.v2.gpu.fnk/}' $video_worker_gpu

									systemctl restart findface-video-worker-gpu.service

									#findface_security
									features_str=""
                                    first=1
                                    for i in ${!features[@]}; do
                                        if [[ $first == 1 ]]; then
                                            features_str="${features[i]}"
                                            first=0
                                            continue
                                        fi
                                       features_str="$features_str, ${features[i]}"
                                    done
									
                                    echo $features_str
									replace "'BODY_EVENTS_FEATURES'" "[$features_str]," $findface_security
									replace "'ENABLE_BODIES'" "True," $findface_security
									systemctl restart findface-security.service
									break
								;;
							esac
						done	
					
					break		
					;;
				esac
			break
			done
		echo -e "\033[36mГотово."
		break
		;;

		delete)
			echo -e "\033[36mКакие атрибуты будем отключать?"
			PS3="Укажите вариант: "
			tput sgr0
			select menu_delete in face car body; do
				case $menu_delete in
				##########################################################_FACE_#######################################################################
					face)
						#clean_features_face
						replace "face_age" "''" $extraction_api
						replace "face_medmask3" "''" $extraction_api
						replace "face_beard" "''" $extraction_api
						replace "face_emotions" "''" $extraction_api
						replace "face_gender" "''" $extraction_api
						replace "face_glasses3" "''" $extraction_api
						systemctl restart findface-extraction-api

						#findface_security
						replace "'FACE_EVENTS_FEATURES'" "[]," $findface_security
						systemctl restart findface-security.service
						break
					;;
				##########################################################_CAR_########################################################################
					car)
						#extraction_api_detectors
						sed -i '/efreitor:/,/.*2048x2048/d' $extraction_api

						#extraction_api_normalizers
						replace2 "carlicplate" "''" $extraction_api
						replace3 "car" "''" $extraction_api

						#extraction_api_models_extractors
						replace "car_description" "''" $extraction_api
						replace "car_emben" "''" $extraction_api
						replace "car_license_plate" "''" $extraction_api
						replace "car_license_plate_quality" "''" $extraction_api
						replace "car_quality" "''" $extraction_api
						replace "car_special_types" "''" $extraction_api
						systemctl restart findface-extraction-api
										
						#video_worker
						sed -i '/^.*CFG_CAR_DETECTOR/{n;s/detector.*/detector = /}' $video_worker_cpu
						sed -i '/^.*CFG_CAR_NORM longopt:/{n;s/norm.*/norm = /}' $video_worker_cpu
						sed -i '/^.*CFG_CAR_QUALITY/{n;s/quality.*/quality = /}' $video_worker_cpu
						sed -i '/^.*CFG_CAR_NORM_QUALITY/{n;s/norm_quality.*/norm_quality = /}' $video_worker_cpu

						sed -i '/^.*CFG_CAR_DETECTOR/{n;s/detector.*/detector = /}' $video_worker_gpu
						sed -i '/^.*CFG_CAR_NORM longopt:/{n;s/norm.*/norm = /}' $video_worker_gpu
						sed -i '/^.*CFG_CAR_QUALITY/{n;s/quality.*/quality = /}' $video_worker_gpu
						sed -i '/^.*CFG_CAR_NORM_QUALITY/{n;s/norm_quality.*/norm_quality = /}' $video_worker_gpu
						systemctl restart findface-video-worker-*.service

						#findface_security
						replace "'CAR_EVENTS_FEATURES'" "[]," $findface_security
						replace "'ENABLE_CARS'" "False," $findface_security
						sed -i "/^.*EXTRA_CAR_MATCHING/{n;s/'color':.*/'color': {'enabled': False, 'min_confidence': 0},/}" $findface_security
						sed -i "/^.*EXTRA_CAR_MATCHING/{n;n;s/'body':.*/'body': {'enabled': False, 'min_confidence': 0},/}" $findface_security
						sed -i "/^.*EXTRA_CAR_MATCHING/{n;n;n;s/'make':.*/'make': {'enabled': False, 'min_confidence': 0},/}" $findface_security
						sed -i "/^.*EXTRA_CAR_MATCHING/{n;n;n;n;s/'model':.*/'model': {'enabled': False, 'min_confidence': 0},/}" $findface_security
						sed -i "/^.*EXTRA_CAR_MATCHING/{n;n;n;n;n;s/'special_vehicle_type':.*/'special_vehicle_type': {'enabled': False, 'min_confidence': 0}/}" $findface_security
						systemctl restart findface-security.service
						break
					;;
				##########################################################_BODY_#######################################################################
					body)
						#clean_detectors
						sed -i '/glenn:/,/.*2048x2048/d' $extraction_api
									
						#extraction_api_normalizers
						replace3 "body" "''" $extraction_api

						#extraction_api_models_extractors
						replace "body_clothes" "''" $extraction_api
						replace "body_color" "''" $extraction_api
						replace "body_emben" "''" $extraction_api
						replace "body_quality" "''" $extraction_api
						systemctl restart findface-extraction-api

						#video_worker_cpu
						sed -i '/^.*CFG_BODY_DETECTOR/{n;s/detector.*/detector = /}' $video_worker_cpu
						sed -i '/^.*CFG_BODY_NORM longopt:/{n;s/norm.*/norm = /}' $video_worker_cpu
						sed -i '/^.*CFG_BODY_QUALITY/{n;s/quality.*/quality = /}' $video_worker_cpu
						sed -i '/^.*CFG_BODY_NORM_QUALITY/{n;s/norm_quality.*/norm_quality = /}' $video_worker_cpu

						#video_worker_gpu
						sed -i '/^.*CFG_CAR_DETECTOR/{n;s/detector.*/detector = /}' $video_worker_gpu
						sed -i '/^.*CFG_CAR_NORM longopt:/{n;s/norm.*/norm = /}' $video_worker_gpu
						sed -i '/^.*CFG_CAR_QUALITY/{n;s/quality.*/quality = /}' $video_worker_gpu
						sed -i '/^.*CFG_CAR_NORM_QUALITY/{n;s/norm_quality.*/norm_quality = /}' $video_worker_gpu

						systemctl restart findface-video-worker-*.service

						#findface_security
						replace "'ENABLE_BODIES'" "False," $findface_security
						replace "'BODY_EVENTS_FEATURES'" "[]," $findface_security										
						systemctl restart findface-security.service
						break
					;;
				esac
			break
			done
		echo -e "\033[36mГотово."
		break
		;;
	esac
break
done
