#!/bin/bash

file="info.txt"
PS3="Выберите действие: "
select menu in add_info check_info clear_info exit; do

	case $menu in
		add_info)
		  echo "Введите имя"
		  read name

		  echo "Введите фамилию"
		  read family

		  echo "Введите возраст"
		  read age

		  comment=""
		  check=1

		  if [[ $family == "Гитлер" && $name == "Адольф" ]]; then
		    comment="\o"
		      elif [[ $age -gt 100 ]]; then
		        comment="врунишка"
		  fi

		  for var in $(cat $file)
		  do
		    if [[ $var == $name";"$family";"$age";"$comment ]]; then
		      echo -e "\033[31mОшибка:Такая запись уже существует"
		      tput sgr0
		      check=0
        #break
		    fi
		  done

		  if [[ $check == 1 ]]; then
		    echo $name";"$family";"$age";"$comment >> info.txt
		  fi
        #break
		;;
		
		check_info)
		  echo -e "\033[36m"
		  cat $file | sed 's/;/ /g'
		  tput sgr0
		#break
		;;

		clear_info)
		  echo "Вы уверены? [Д]/[Н]"
		  read question
		  if [[ $question ==  "Д" ]]; then
		    > $file
		  break
		  elif [[ $question ==  "Н" ]]; then
		    echo "Определись уже"
		  else
		  echo -e "\033[31mОшибка:Некорректный запрос"
		  tput sgr0
		  fi
		;;
      
      	*) 
      	echo -e "\033[31mОшибка:Некорректный запрос"
      	tput sgr0
      	;;
		
		exit)
		break
		;;
	esac

done

